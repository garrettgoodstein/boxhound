import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { ReactiveFormsModule } from '@angular/forms';

import { AgmCoreModule } from '@agm/core';

import { ProjectDomino } from './app.component';

import * as firebase from 'firebase/app';

import { Camera } from '@ionic-native/camera';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
// import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AuthService } from '../providers/auth';
import { BoxService } from '../providers/box';
import { UtilityService } from '../providers/utility';
import { LocationService } from '../providers/location';

import { AddBoxPage } from '../pages/addBox/addBox';
import { BoxDetailsPage } from '../pages/boxDetails/boxDetails';
import { LocationFormPage } from '../pages/location-form/location-form';
import { LocationListPage } from '../pages/location-list/location-list';
import { LoginPage } from '../pages/login/login';
import { ScanPage } from '../pages/scan/scan';
import { SearchPage } from '../pages/search/search';
import { SettingsPage } from '../pages/settings/settings';
import { TabsPage } from '../pages/tabs/tabs';

import { FilterByNamePipe } from '../pipes/filterByName';
import { SortByNamePipe } from '../pipes/sortByName';

// Initialize Firebase
const config = {
  apiKey: "AIzaSyD3dk95KY7eQUPKsWq1gb5_64TuUVi5eDs",
  authDomain: "boxhound-f3fb1.firebaseapp.com",
  databaseURL: "https://boxhound-f3fb1.firebaseio.com",
  projectId: "boxhound-f3fb1",
  storageBucket: "boxhound-f3fb1.appspot.com",
  messagingSenderId: "643393424063"
};
firebase.initializeApp(config);

@NgModule({
  declarations: [
    ProjectDomino,
    AddBoxPage,
    BoxDetailsPage,
    LoginPage,
    LocationFormPage,
    LocationListPage,
    ScanPage,
    SearchPage,
    SettingsPage,
    TabsPage,
    FilterByNamePipe,
    SortByNamePipe
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(ProjectDomino),
    AngularFireModule.initializeApp(config),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDzbfhNgfbnm-X2oA55qPat4oXfNoY7rrc',
      libraries: ['places']
    }),
    ReactiveFormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    ProjectDomino,
    AddBoxPage,
    BoxDetailsPage,
    LoginPage,
    LocationFormPage,
    LocationListPage,
    ScanPage,
    SearchPage,
    SettingsPage,
    TabsPage
  ],
  providers: [
    Camera,
    InAppBrowser,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthService,
    BoxService,
    UtilityService,
    LocationService,
    BarcodeScanner,
    // QRScanner
  ]
})
export class AppModule {}
