import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByName',
  pure: false
})
export class FilterByNamePipe implements PipeTransform {
  transform(arrayValues: any[], filterValue: string): any {
    if (!arrayValues) {
      return [];
    } else if(filterValue) {
      let box: Object;
      for(let i = 0; i < arrayValues.length; i++) {
        console.log(arrayValues[i]);
        
      }

      return arrayValues.filter(item => {
        item.boxes.filter(box => {
          return box.name.toLowerCase().startsWith(filterValue.toLowerCase());
        })
      });
    } else {
      return arrayValues;
    }
    
  }
}
