import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortByName',
  pure: false
})
export class SortByNamePipe implements PipeTransform {
  transform(arrayValues: any[]): any {
    return arrayValues.concat().sort((item1: any, item2: any): number => {
      if (item1.name.toLowerCase() < item2.name.toLowerCase()) {
        return -1;
      } else if (item1.name.toLowerCase() > item2.name.toLowerCase()) {
        return 1;
      }
      return 0;
    });
  }
}
