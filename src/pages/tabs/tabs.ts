import { Component, NgZone } from '@angular/core';

import { AddBoxPage } from '../addBox/addBox';
import { ScanPage } from '../scan/scan';
import { SearchPage } from '../search/search';
import { SettingsPage } from '../settings/settings';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = SearchPage;
  tab2Root = AddBoxPage;
  tab3Root = ScanPage;
  tab4Root = SettingsPage;

  constructor(private zone: NgZone) { }

  changeTab(): void {
    // Force UI to update
    // Was getting issue where it sometimes does not update to the correct tab
    this.zone.run(()=> {});
  }
}
