import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LocationService } from '../../providers/location';
import { LocationFormPage } from '../location-form/location-form';

@IonicPage()
@Component({
  selector: 'page-location-list',
  templateUrl: 'location-list.html',
})
export class LocationListPage {

  constructor(
    public locationService: LocationService,
    public navCtrl: NavController, 
    public navParams: NavParams
  ) 
  {
    this.locationService.getLocationsFromDb();
  }

  goToLocationForm(location: Object) {
    console.log(location);
    this.navCtrl.push(LocationFormPage, {location: location});
  }

}
