import { Component } from '@angular/core';
import { LoadingController, NavController, NavParams, ModalController } from 'ionic-angular';

import { SearchPage } from '../search/search';
import { AddBoxPage } from '../addBox/addBox';
import { BoxService } from '../../providers/box';
import { UtilityService } from '../../providers/utility';

import * as _ from 'lodash';

@Component({
  selector: 'page-box-details',
  templateUrl: 'boxDetails.html'
})

export class BoxDetailsPage {
  box: any;
  boxContents: string[] = [];
  boxContentKeys: string[] = [];

  constructor(public navCtrl: NavController,
              public loadingCtrl: LoadingController,
              public modalCtrl: ModalController,
              private navParams: NavParams,
              private _util: UtilityService,
              private _boxService: BoxService) {
    this.box = this.navParams.data.box;

    if (this.box.contents) {
      this.boxContentKeys = Object.keys(this.box.contents);
      this.getBoxContent();      
      console.log(this.box);
    }
  }

  ionViewWillEnter() {
    if (this.box.$key) {
      this._boxService.listenForBoxUpdates(this.box.$key).subscribe(box => {
        this.box = box;
      })
    }
  }

  getBoxContent() {
    for(let i = 0; i < this.boxContentKeys.length; i++) {
      let key = this.boxContentKeys[i];
      this.boxContents.push(this.box.contents[key]);
    }
  }
  
  goToAddBox(box) {
    console.log('Here is box: ', box);
    // this.navCtrl.push(AddBoxPage, {box: box, boxContents: this.boxContents});
    let addBoxModal = this.modalCtrl.create(AddBoxPage, {box: box, boxContents: this.boxContents});
    addBoxModal.present();
  }

  close(): void {
    this.navCtrl.pop();
  }

}
