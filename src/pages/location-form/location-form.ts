import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup} from '@angular/forms';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MapsAPILoader } from '@agm/core';

import { LocationService } from '../../providers/location';
import { UtilityService } from '../../providers/utility';

declare var google: any;

@IonicPage()
@Component({
  selector: 'page-location-form',
  templateUrl: 'location-form.html',
})
export class LocationFormPage {
  @ViewChild("search") 
  public searchElementRef: ElementRef;
  public search: ElementRef;
  name: string;
  currentAddress: string;
  isCurrentLocation: boolean =  false;
  receivedLocation: any;
  isEditing: boolean = false;
  zoom: number = 9;
  lat: number = 44.974033;
  lng: number = -93.275664;
  locationObject = {
    place_id: '',
    location_name: '',
    location_info: {},
    lat: '',
    lng: '',
    street_address: '',
    formatted_address: ''
  }
  locationForm = {
    street_number: 'short_name',
    route: 'short_name',
    locality: 'short_name',
    administrative_area_level_1: 'short_name',
    // country: 'long_name',
    postal_code: 'short_name'
  };
  isValidLocation: boolean;

  constructor(
    public mapsAPILoader: MapsAPILoader, 
    public navCtrl: NavController, 
    public navParams: NavParams,
    public ngZone: NgZone,
    public locationService: LocationService,
    public utilityService: UtilityService
  ) {
    this.receivedLocation = this.navParams.get('location');

    if(this.receivedLocation) {
      this.locationObject = {
        place_id: this.receivedLocation.place_id,
        location_name: this.receivedLocation.location_name,
        location_info: {
          street_number: this.receivedLocation.location_info.street_number,
          route: this.receivedLocation.location_info.route,
          locality: this.receivedLocation.location_info.locality,
          administrative_area_level_1: this.receivedLocation.location_info.administrative_area_level_1,
          postal_code: this.receivedLocation.location_info.postal_code
        },
        lat: this.receivedLocation.lat,
        lng: this.receivedLocation.lng,
        street_address: this.receivedLocation.street_address,
        formatted_address: this.receivedLocation.formatted_address
      }
      
      this.isEditing = true;
      this.name = this.receivedLocation.location_name,
      this.currentAddress = this.receivedLocation.formatted_address
    }
  }

  ionViewDidLoad() {
    this.mapsAPILoader.load().then(() => {

      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        location: 'this.lat,this.lng'
      })

      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
        let place = autocomplete.getPlace();

        //verify result
        if (place.geometry === undefined || place.geometry === null) {
          return;
        }
        
        for (let i = 0; i < place.address_components.length; i++) {
          let addressType = place.address_components[i].types[0];
          if (this.locationForm[addressType]) {
            let val = place.address_components[i][this.locationForm[addressType]];
            this.locationForm[addressType] = val;     
          } 
        }

        this.locationObject.place_id = place.place_id;
        this.locationObject.street_address = place.name;
        this.locationObject.location_info = this.locationForm;
        this.locationObject.lat = place.geometry.location.lat();
        this.locationObject.lng = place.geometry.location.lng();
        this.locationObject.formatted_address = place.formatted_address;
        
        console.log('**Here is locationObject: ', this.locationObject, 'Is this editing? ', this.isEditing);
        
        //set latitude, longitude and zoom
        this.isCurrentLocation = true;
        this.lat = place.geometry.location.lat();
        this.lng = place.geometry.location.lng();
        this.zoom = 12;

        console.log('**Here is lat, long, zoom', this.lat, this.lng, this.zoom);
        
      });
    });
 
    })
  }

  clearInput() {
    this.currentAddress = '';
  }

  checkForEmptyInput(locationObject: any) {
    console.log(locationObject);
    if (locationObject.place_id === '') {
      this.utilityService.showToast('Not a valid location');
      this.isValidLocation = false;
    } else {
      this.isValidLocation = true;
    }
  }

  saveLocation() {
      this.locationObject.location_name = this.name ? this.name : this.locationObject.formatted_address;
      this.locationService.saveLocationToDb(this.locationObject)
      .then(() => this.utilityService.showToast('Successfully added new location'));
      // tells add box page that a location has been added so location input is not empty
      this.navCtrl.getPrevious().data.addedLocation = true;
      this.navCtrl.pop();
  }

  updateLocation() {
    this.checkForEmptyInput(this.locationObject);
    if (this.name === '') {
      this.utilityService.showToast('Please enter a location name');
      this.isValidLocation = false;
    }
    if (this.isValidLocation) {
      this.locationObject.location_name = this.name;
      let key = this.receivedLocation.$key;
      this.locationService.updateLocationInDb(this.locationObject, key);
      // tells add box page that a location has been added so location input is not empty
      this.navCtrl.getPrevious().data.addedLocation = true;
      this.navCtrl.pop();
    }
  }

}
