import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LocationFormPage } from './location-form';
import { AgmCoreModule } from '@agm/core';


@NgModule({
  declarations: [
    LocationFormPage,
  ],
  imports: [
    IonicPageModule.forChild(LocationFormPage),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDzbfhNgfbnm-X2oA55qPat4oXfNoY7rrc',
      libraries: ['places']
    })
  ],
  exports: [
    LocationFormPage
  ]
})
export class LocationFormPageModule {}
