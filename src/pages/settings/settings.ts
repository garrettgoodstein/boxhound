import { Component } from '@angular/core';
import { App, NavController } from 'ionic-angular';

import { InAppBrowser } from '@ionic-native/in-app-browser';

import { AuthService } from '../../providers/auth';

import { LoginPage } from '../login/login';
import { LocationListPage } from '../location-list/location-list';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  constructor(private app: App,
              public navCtrl: NavController,
              private iab: InAppBrowser,
              private _auth: AuthService) {
  }

  openTerms(): void {
    const browser = this.iab.create('http://www.smead.com/Director.aspx?NodeID=978');
    browser.show();
    // browser.executeScript(...);
  }

  editLocation() {
    this.navCtrl.push(LocationListPage);
  }

  logout(): void {
    this._auth.logout();
    this.app.getRootNav().setRoot(LoginPage);
  }
}
