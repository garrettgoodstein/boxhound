import { Component, ChangeDetectorRef, NgZone, ViewChildren, ElementRef, QueryList, Renderer2 } from '@angular/core';
import { LoadingController, ItemSliding, NavController, NavParams } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';

import { AuthService } from '../../providers/auth';

import * as firebase from 'firebase';

import { SearchPage } from '../search/search';
import { LocationFormPage } from '../location-form/location-form';
import { BoxService } from '../../providers/box';
import { UtilityService } from '../../providers/utility';
import { LocationService } from '../../providers/location';

import { BoxhoundBox } from '../../declarations';
import { LOCATION_INITIALIZED } from '@angular/common';

@Component({
  selector: 'page-add-box',
  templateUrl: 'addBox.html'
})

export class AddBoxPage {
  @ViewChildren('contentInput') contentInputElement: QueryList<any>;
  box: BoxhoundBox;
  boxContents: string[];
  imgBase64: any = {
    contents: null,
    location: null
  };
  isNewBox: boolean = true;
  addNewLocation: string = 'Add New Location..';
  showDelete: boolean = true;
  addedLocation: boolean;
  boxKeyOnPageEnter: string;

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    private zone: NgZone,
    private _ref: ChangeDetectorRef,
    private _util: UtilityService,
    private _boxService: BoxService,
    private camera: Camera,
    private _auth: AuthService,
    public locationService: LocationService,
    private _zone: NgZone,
    public navParams: NavParams,
    public renderer2: Renderer2
  ) {
    
    this.locationService.getLocationsFromDb();
    this.box = this.navParams.get('box');

    this.boxContents = this.navParams.get('boxContents');

    if (this.box) {
      this.isNewBox = false;
      this.boxKeyOnPageEnter = this.box.locationKey;
    } else {
      this.box = new BoxhoundBox;
    }

  }

  ionViewWillEnter() {
    this.addedLocation = this.navParams.get('addedLocation') || false;
    
    if (!this.addedLocation && this.isNewBox) {
      this.box.locationKey = null;
      this.boxContents = [''];
    } else if (!this.addedLocation && !this.isNewBox && this.boxKeyOnPageEnter !== '0000') {
      this.box.locationKey = this.boxKeyOnPageEnter;
    } else if (!this.addedLocation && !this.isNewBox) {
      this.box.locationKey = null;
    } else if (this.isNewBox) {
      this.boxContents = [''];
    }
  }

  goToLocationForm() {
    this.navCtrl.push(LocationFormPage);
  }

  // Open phone's camera
  // If user takes / selects a photo, update the imgBase64 object accordingly
  // After the user saves the box data, we'll save the base64 (from imgBase64{})
  // since we need to know the box id first
  takePhoto(photoType): void {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType : this.camera.PictureSourceType.CAMERA,
      encodingType: this.camera.EncodingType.PNG,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 200,
      targetHeight: 200
    };

    this.camera.getPicture(options).then((imageData) => {
      // Issue with img not updating after being taken
      // Hacked together for zone.run() for now.
      this.zone.run(()=> {
        this.imgBase64[photoType] = imageData;
      });
    }, (err) => {
     console.log('Error taking picture:', err);
    });
  }

  getBase64(base64String: string): string {
    return 'data:image/jpeg;base64,' + base64String;
  }

  // Save a photo in the following Firebase Storage path: /userId/boxId/[photoType]
  // photoType is either 'contents' or 'location'
  savePhoto(boxData, boxKey): any {
    let storageRef = firebase.storage().ref();

    // Create a reference to [photoType].jpg
    let contentsRef = storageRef.child(this._auth.getUser().uid).child(boxKey).child('contents.jpg');
    let locationRef = storageRef.child(this._auth.getUser().uid).child(boxKey).child('location.jpg');

    // Get the image path
    // e.g. userId/boxId/contents.jpg

    //DataUrl refers to the download links
    //ImgUrl refers to the src to be used in img tags
    let urlData = {
      boxKey: boxKey,
      contentsDataUrl: boxData.contentsDataUrl || null,
      locationDataUrl: boxData.locationDataUrl || null,
      contentsImgUrl: boxData.contentsImgUrl || null,
      locationImgUrl: boxData.locationImgUrl || null
    };

    // this.imgBase64['contents'] = 'asdfasdfasdfasf';
    // this.imgBase64['location'] = 'ertyui';

    return new Promise((resolve, reject) => {
      let promises = [];

      //Save the download links
      if (this.imgBase64.contents) {
        urlData.contentsDataUrl = contentsRef.fullPath;
        promises.push(
          contentsRef.putString(this.imgBase64.contents, 'base64')
          .then(() => { return contentsRef.getDownloadURL(); })
          .then(url => urlData.contentsImgUrl = url)
        );
      }
      if (this.imgBase64.location) {
        urlData.locationDataUrl = locationRef.fullPath;
        promises.push(
          locationRef.putString(this.imgBase64.location, 'base64')
          .then(() => { return locationRef.getDownloadURL(); })
          .then(url => urlData.locationImgUrl = url)
        );
      }

      Promise.all(promises).then(() => {
        resolve(urlData)
      });
    });
  }

  /**
   * Add scanned QR code data to box
   */
  addQRCode() {
    this._util.showCodeScanner()
    .then((barcodeData) => {
      this.box.qrCode = barcodeData.text;
     }, (err) => {
        console.log(err);
     });;
  }

  saveBox(): void {
    if (!this.box.name) {
      this.box.name = 'Untitled Box';
    }
    
    if (this.box.name) {
      if (this.box.locationKey === this.addNewLocation) {
        this._util.showToast('Please select a valid location');
        return;
      } else if (!this.box.locationKey || this.box.locationKey === '') {
        this.box.locationKey = '0000';
      }

      // If No Location...
      
      let loader = this.loadingCtrl.create({});
      loader.present();
      let boxKey = this.box.$key;
      let dataCopy = Object.assign({}, this.box);
      let contentsCopy = {};
      
      //Transform array of box contents into an object for firebase
      this.boxContents.forEach((item) => {
        let ref = firebase.database().ref().push();
        let boxContentKey = ref.key;
        
        //Don't add empty keys to firebase
        if (item && item.length > 0) {
          contentsCopy[boxContentKey] = item;
        }
      });
      dataCopy.contents = contentsCopy;

      if(this.isNewBox) {
        console.log('Key:',  firebase.database().ref().push().key);
        boxKey = firebase.database().ref().push().key;
        this._boxService.updateBox(dataCopy, boxKey)
        .then(snapshot => this.savePhoto(dataCopy, boxKey))
        .then(updateData => this._boxService.updateUrls(updateData, boxKey))
        .then(() => {
          loader.dismiss();
          this._util.showToast('Box saved!');
          this.clearData();
          this.navCtrl.parent.select(0);
        })
        .catch((error) => {
          loader.dismiss();
          this._util.showToast('Oops, there was an issue saving this box. Please try again.');
          console.log(error);
        });
      } else {
        this._boxService.updateBox(dataCopy, boxKey)
        .then(snapshot => this.savePhoto(dataCopy, boxKey))
        .then(updateData => this._boxService.updateUrls(updateData, boxKey))
        .then(() => {
          loader.dismiss();
          this._util.showToast('Box saved!');
          this.clearData();
          this.navCtrl.pop();
        })
        .catch((error) => {
          loader.dismiss();
          this._util.showToast('Oops, there was an issue saving this box. Please try again.');
          console.log(error);
        });
      }
    }
  }

  locationInputChange() {
    if(this.box.locationKey === this.addNewLocation) {
      this.goToLocationForm();
    }
  }

  addBoxContent(boxItems): void {
    this.zone.run(()=> {
      this.boxContents.push('');
    });

    //set focus to last element in this.boxContents array -- In Progress
    // this.contentInputElement.last.nativeElement.focus()
  }

  removeBoxContents(contentIndex: any, slideItem: ItemSliding): void {
    this.boxContents.splice(contentIndex, 1);
    slideItem.close();
  }

  clearData(): void {
    this.box = new BoxhoundBox;
    this.imgBase64 = {
      contents: null,
      location: null
    };
  }

  customTrackBy(index: number, obj: any): any {
    return index;
  }

  close(): void {
    this.navCtrl.pop();
  }

}
