import { Component, OnInit } from '@angular/core';
import { App, NavController } from 'ionic-angular';

import { SearchPage } from '../search/search';
import { UtilityService } from '../../providers/utility';

import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';

@Component({
  selector: 'page-scan',
  templateUrl: 'scan.html'
})
export class ScanPage {

  constructor(public app: App, public navCtrl: NavController, public barcodeScanner: BarcodeScanner, public utilityService: UtilityService) {
  }

  ionViewWillEnter() {
    //launch code scanner when user navigates to qr scanner tab everytime
    this.utilityService.showCodeScanner()
    .then((barcodeData) => {
      console.log(barcodeData);
      // if (barcodeData.text === '' || undefined) {

      // }
      this.utilityService.qrCode = barcodeData.text;
      //if barcode data is found on a box or boxes, navigate to Search Page to show those boxes
      this.navCtrl.parent.select(0);
     }, (err) => {
        console.log(err);
        this.utilityService.showToast('Error scanning QR Code. Please try again.')
        //if no qr codes are found, also navigate to Search page and display toast to have user try again
        this.navCtrl.parent.select(0);     
    });;
  }


}
