import { Component, ChangeDetectorRef, NgZone } from '@angular/core';
import { ModalController, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

import { AuthService } from '../../providers/auth';
import { BoxService } from '../../providers/box';
import { LocationService } from '../../providers/location';
import { UtilityService } from '../../providers/utility';

import { BoxDetailsPage } from '../boxDetails/boxDetails';
import { LocationListPage } from '../location-list/location-list';

import * as firebase from 'firebase';
import * as _ from 'lodash';

@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  items: FirebaseListObservable<any[]>;
  sortedItems: any[] = [];
  preFilteredItems: any[] = [];
  searchTerm: string = '';
  foundQRCode: boolean = false;
  isDoingQRFilter: boolean = false;

  constructor(public navCtrl: NavController,
              public modalCtrl: ModalController,
              private _changeDetector: ChangeDetectorRef,
              private _zone: NgZone,
              private _auth: AuthService,
              private db: AngularFireDatabase,
              private _boxService: BoxService,
              private locationService: LocationService,
              public navParams: NavParams,
              public utilityService: UtilityService) {
    this._zone.run(() => this.items = db.list('/boxes/' + _auth.getUser().uid));
    this.locationService.getLocationsFromDb();
    // this._boxService.getMyBoxes()
  }

  ionViewWillEnter() {
    this.foundQRCode = false;
    const boxQRCode = this.utilityService.qrCode;
    this.sortBoxesByLocation(boxQRCode);
    if (boxQRCode) {
      this.isDoingQRFilter = true;
    } 
    // clear out qrCode once boxes with that qr code are found
    this.utilityService.qrCode = null;

  }

  sortBoxesByLocation(boxQRCode) {
    let currentKey;
    let locationName;
    this.items.subscribe(
      response => {
        console.log(response);
        this.sortedItems = [];
        
        for(let i = 0; i < response.length; i++) {
          const box = response[i];
          let locationObject;

          locationObject = box.locationKey === '0000' ? {location_name: 'Unassigned', boxes: [], $key: box.locationKey} : this.locationService.getLocationFromMemory(box.locationKey);

          if (boxQRCode) {
            //if you get a boxQRCode from scan and there is a qrCode on the box that is currently being looped over, then continue the for loop.
            if (!box.qrCode || box.qrCode !== boxQRCode) {
              continue;
            } else if (box.qrCode === boxQRCode) {
              this.foundQRCode = true;
            } 
          }

          //find all keys associated with the current location key
          currentKey = _.find(this.sortedItems, {locationKey: locationObject.$key})

          // If a section for the current location has already been created, push current box into that locations box array. 
          // Else, create a new location section and push current box into its new array
          if (currentKey) {
            currentKey.boxes.push(response[i]);           
          } else {
            this.sortedItems.push({
              locationKey: locationObject ? locationObject.$key : null, 
              locationObject: locationObject ? locationObject : {location_name: locationName}, 
              boxes: [response[i]]
            });   
          }     
          
          this.preFilteredItems = this.sortedItems;
          console.log('Boxes sorted by location: ', this.preFilteredItems);
        }
      },
      error => console.log(error)     
    )
  }

  //when user scans a qrcode and finds boxes with that qr code, allow the user to remove the filter of scanned boxes and display all of the boxes again
  removeFilter() {
    this.isDoingQRFilter = false;
    this.sortBoxesByLocation(null);
  }

  /**
   * Fires whenever the search bar updates; update the sortedItems with the filter
   */
  searchUpdate(): void {
    console.log('searchTerm', this.searchTerm);

    if (this.searchTerm.length === 0) {
      this.sortedItems = this.preFilteredItems;
      // this._changeDetector.detectChanges();
      return;
    }

    // Need to clone the original data to not have the filter applied to it
    let clonedPrefilteredItems = _.cloneDeep(this.preFilteredItems);
    let holderArr: any[] = [];

    for (let i = 0; i < clonedPrefilteredItems.length; i++) {
      const location = clonedPrefilteredItems[i];
      const locationsFilteredBoxes: any[] = [];

      if (!location.boxes || !location.boxes.length) {
        continue;
      }

      for (let k = 0; k < location.boxes.length; k++) {
        const box = location.boxes[k];

        if (box.name.toLowerCase().startsWith(this.searchTerm.toLocaleLowerCase())) {
          locationsFilteredBoxes.push(box);
        }
      }

      if (locationsFilteredBoxes.length) {
        location.boxes = locationsFilteredBoxes;
        holderArr[holderArr.length] = location;
      }
    }

    this.sortedItems = holderArr;
    // this.items = this.items;
    // this._changeDetector.detectChanges();
  }
  
  goToBox(box, item): void {
    console.log('Here is box: ', box, 'Here is location: ', item);
    box.locationObject = item.locationObject;
    this.navCtrl.push(BoxDetailsPage, {box: box});
    
    // let boxDetailsModal = this.modalCtrl.create(BoxDetailsPage, {box: box});
    // boxDetailsModal.present();
  }

  goToAddBox(): void {
    this.navCtrl.parent.select(1);
  }

  goToEditLocation() {
    this.navCtrl.push(LocationListPage);
  }
}
