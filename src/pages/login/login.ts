import { Component } from '@angular/core';
import { App, LoadingController, ToastController } from 'ionic-angular';

import { AuthService } from '../../providers/auth';
import { UtilityService } from '../../providers/utility';

import { TabsPage } from '../tabs/tabs';

import * as firebase from 'firebase/app';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  credentials = {
    email: null,
    password: null
  };
  confirmPass: string;
  workflow = 'LOGIN'; // Or 'Register'
  switchText = "Don't have an account?";
  triggerWord = 'Register here';

  constructor(public app: App,
              public loadingCtrl: LoadingController,
              private _auth: AuthService,
              private _utility: UtilityService) {
    this.waitForAuthInitialization();
  }

  waitForAuthInitialization(): void {
    let loader = this.loadingCtrl.create({});
    loader.present();

    firebase.auth().onAuthStateChanged(user => {
      loader.dismiss();

      if (user) {
        // User is signed in.
        console.log('user', user);
        this.proceedToApp();
      } else {
        // No user is signed in.
      }
    });
  }

  switchWorkflow(): void {
    if (this.workflow === 'LOGIN') {
      this.workflow = 'REGISTER';
      this.switchText = 'Already have an account?';
      this.triggerWord = 'Log in';
    } else {
      this.workflow = 'LOGIN';
      this.switchText = "Don't have an account?";
      this.triggerWord = 'Register here';
    }
  }

  doAuth(): void {
    if (!this.credentials.email || !this.credentials.password) {
      return;
    }


    if (this.workflow === 'LOGIN') {
      this.login();
    } else {
      if (this.credentials.password !== this.confirmPass) {
        console.log(this.credentials.password, this.confirmPass); 
        this._utility.showToast('Passwords do not match.');
        this.confirmPass = '';
      } else {
        this.register();
      }

    }
  }

  //ngIf add confirm password for registration and validate against other password

  login(): void {
    let loader = this.loadingCtrl.create({});
    loader.present();
    this._auth.login(this.credentials.email, this.credentials.password)
    .then((result) => {
      console.log(result);
      loader.dismiss();
      this.proceedToApp();
    }).catch((error) => {
      console.log(error);
      loader.dismiss();
      this._utility.showToast(error.message);
    });
  }

  register(): void {
    let loader = this.loadingCtrl.create({});
    loader.present();
    this._auth.register(this.credentials.email, this.credentials.password)
    .then((result) => {
      console.log(result);
      loader.dismiss();
      this.proceedToApp();
    }).catch((error) => {
      console.log(error);
      loader.dismiss();
      this._utility.showToast(error.message);
    });
  }

  proceedToApp(): void {
    this.app.getRootNav().setRoot(TabsPage);
  }
}
