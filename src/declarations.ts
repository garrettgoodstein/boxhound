/*
  Declaration files are how the Typescript compiler knows about the type information(or shape) of an object.
  They're what make intellisense work and make Typescript know all about your code.

  A wildcard module is declared below to allow third party libraries to be used in an app even if they don't
  provide their own type declarations.

  To learn more about using third party libraries in an Ionic app, check out the docs here:
  http://ionicframework.com/docs/v2/resources/third-party-libs/

  For more info on type definition files, check out the Typescript docs here:
  https://www.typescriptlang.org/docs/handbook/declaration-files/introduction.html
*/
// declare module '*';

export class BoxhoundBox {
  constructor (
    public name: string = '',
    public locationKey: string = '',
    public contents: any = null,
    public boxKey: string = null,
    public contentsImgUrl: string = null,
    public locationImgUrl: string = null,
    public qrCode: string = '',
    public $key?: string
  ) {}
} 

export interface BoxhoundLocation {
  formatted_address: string,
  lat: number,
  lng: number, 
  location_info: any,
  location_name: string,
  place_id: string,
  street_address: string
}
