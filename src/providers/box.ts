import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

import { AuthService } from './auth';
import { UtilityService } from './utility';

import * as firebase from 'firebase/app';
import * as _ from 'lodash';
import { BoxhoundBox } from '../declarations';


@Injectable()
export class BoxService {
  constructor(
    public db: AngularFireDatabase,
    private _auth: AuthService,
    private _util: UtilityService) {
  }

  listenForBoxUpdates(boxKey: string) {
    return this.db.object(`boxes/${this._auth.getUser().uid}/${boxKey}`);
    // return firebase.database().ref('boxes').child(this._auth.getUser().uid).child(boxKey);
  }
  
  updateBox(boxData: BoxhoundBox, boxKey: string): any {
    const clonedBox = this.cleanBoxData(boxData);
    return firebase.database().ref('boxes').child(this._auth.getUser().uid).child(boxKey).update(clonedBox);
  }

  /**
   * Clean auxiliary data that messes up Firebase operations
   */
  cleanBoxData(boxData: BoxhoundBox): any {
    const clonedBox = _.cloneDeep(boxData);
    delete clonedBox.$key;
    delete clonedBox.locationObject;    

    if (Object.keys(clonedBox.contents).length === 0) {
      delete clonedBox.contents;
    }

    return clonedBox;
  }

  updateUrls(updateData: any, boxKey: string): any {
    console.log('updating data');
    for (let key in updateData) {
      console.log('key', key);
      console.log('value', updateData[key]);
    }
    return firebase.database().ref('boxes')
    .child(this._auth.getUser().uid)
    .child(boxKey)
    .update(updateData);
  }

}
