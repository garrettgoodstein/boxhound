import { Injectable } from '@angular/core';

import { ToastController } from 'ionic-angular';

// import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';

@Injectable()
export class UtilityService {
  qrCode: string;

  constructor(public toastCtrl: ToastController, public barcodeScanner: BarcodeScanner) { }

  showToast(msg): void {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  showCodeScanner(): any {
    return this.barcodeScanner.scan({disableSuccessBeep: true})
  }



}
