import { Injectable } from '@angular/core';

import * as firebase from 'firebase/app';

@Injectable()
export class AuthService {

  constructor() {
  }

  getUser(): any {
    let user = firebase.auth().currentUser;

    if (user) {
      return user;
    } else {
      return null;
    }
  }

  register(email, password): any {
    return firebase.auth().createUserWithEmailAndPassword(email, password);
  }

  login(email, password): any {
    return firebase.auth().signInWithEmailAndPassword(email, password);
  }

  logout(): void {
    firebase.auth().signOut().then(() => {

    }).catch(function(error) {
      console.log('Error logging out', error);
    });
  }
}
