import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AuthService } from './auth';

import * as firebase from 'firebase/app';
import * as _ from 'lodash';

@Injectable()
export class LocationService {
  myLocations: FirebaseListObservable<any[]>;
  myActualLocations: any[];

  constructor(
    private _auth: AuthService, 
    public db: AngularFireDatabase, 
    ) {
  }

  saveLocationToDb(locationObject) {
    console.log(locationObject);
    return firebase.database().ref('userLocations').child(this._auth.getUser().uid).push(locationObject);
  }

  updateLocationInDb(locationObject, key) {
    console.log(locationObject);
    return firebase.database().ref('userLocations').child(this._auth.getUser().uid).child(key).update(locationObject);
  }

  getLocationFromMemory(locationKey): any {
    return _.find(this.myActualLocations, {$key: locationKey});
  }

  getLocationsFromDb(): FirebaseListObservable<any[]> {
    this.myLocations = this.db.list(`/userLocations/${this._auth.getUser().uid}`);
    this.myLocations.subscribe(
      response => { this.myActualLocations = response; console.log(this.myActualLocations); },
      error => console.log(error)
    )
    return this.myLocations;
  }

}
